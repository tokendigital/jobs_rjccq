<?php
/**
 *
 */
?>

<div class="job_listings-map-wrapper">
	<?php do_action( 'jobify_map_before' ); ?>

	<div class="job_listings-map">
		<div id="job_listings-map-canvas"></div>
	</div>

	<?php do_action( 'jobify_map_after' ); ?>
</div>