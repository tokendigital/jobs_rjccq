<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'admin_jobddb');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'admin_jobuse');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'HyprAct1f');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '00IgC@kZhnYpC{-cd&8bYXzv;#XKdc%S/<)`Y.:$42;&Pr0;Z0j8+N6+5N]-(cJX&Q');
define('SECURE_AUTH_KEY',  '00.L2U46Z/E%0p@L8?[S.8c2mX.J{)|cU+HX/AzVz(.WA/AT-=GER4Ujv pAP:`2u/');
define('LOGGED_IN_KEY',    '00N@;K/n!b0FFk8ZeQ|7q|C4x,Dfe2e@FRfMe],TYGCX&){cH?Ej&C;Zi5|&F}PWA+');
define('NONCE_KEY',        '00IPG3Ut_5A7Ohq(qY|vM_tw73I(9~HS.|AQr#%[B<]6#HpZ9Op|KzqWrD@JF2&#yz');
define('AUTH_SALT',        '00Rug`%-8{;Eya^+1NgWiT$bxNZ_s|xE>!n@.rkQ<R[L[8/ j@-V-YJQkW&AKx:1%V');
define('SECURE_AUTH_SALT', '00t@@[)FUkevj}.NZ-]uo.mb}++#$QY_)iaYzS+]v[[zVGajuSi,:F85=L6YSS_aYW');
define('LOGGED_IN_SALT',   '00t|ekxef?+@A+}C*>F^b^h#|0 =RWxI!Z8wWk<A6T?r+F+*I>RW-Qe-x`Bn={.A9o');
define('NONCE_SALT',       '00]|Pw9,=O+4)=/}lokd<!(3:+9SGhOjS|^/lv/Yu;*%A~n#SMuC;riW)v(1|.>,f|');
/**#@-*/


/* WordPress Localized Language. */
define( 'WPLANG', 'fr_FR' );


/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 


/** 
*
*     FTP
*
*/

define( 'FS_METHOD', 'ftpext' );
define( 'FTP_BASE', '/var/www/vhosts/codeactif.com/job' );
define( 'FTP_CONTENT_DIR', '/var/www/vhosts/codeactif.com/job/wp-content/' );
define( 'FTP_PLUGIN_DIR ', '/var/www/vhosts/codeactif.com/job/wp-content/plugins/' );
define( 'FTP_USER', 'job' );
define( 'FTP_PASS', 'HyprAct1f' );
define( 'FTP_HOST', 'codeactif.com' );
define( 'FTP_SSL', false );




/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');